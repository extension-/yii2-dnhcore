My extention core
=================
My extention core Description

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist duongnh/yii2-dnhcore "*"
```

or add

```
"duongnh/yii2-dnhcore": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \duongnh\dnhcore\AutoloadExample::widget(); ?>```